window.addEventListener('load', () => {
    const letters = document.querySelectorAll('.letter');
    let validLetters = [];
    const livesText = document.querySelector('#lives');
    letters.forEach(letter => {
        validLetters.push(letter.getAttribute('data-text'));
    });
    document.addEventListener('keydown', function(event) {
        if (parseInt(livesText.innerText) > 0) {
            if (validLetters.includes(event.key)) {
                letters.forEach(letter => {
                    if (letter.getAttribute('data-text') === event.key) {
                        letter.innerText = letter.getAttribute('data-text').toUpperCase();
                    }
                });
            } else {
                livesText.innerText = parseInt(livesText.innerText) - 1;
                if (parseInt(livesText.innerText) < 5 && parseInt(livesText.innerText) > 0) {
                    livesText.classList.add('bg-rose-500');
                    livesText.classList.remove('bg-green-300');
                } else if (parseInt(livesText.innerText) > 5) {
                    livesText.classList.add('bg-green-300');
                    livesText.classList.remove('bg-rose-500');
                } else if (parseInt(livesText.innerText) === 0) {
                    livesText.classList.add('bg-rose-500');
                    livesText.classList.remove('bg-green-300');
                    letters.forEach(letter => {
                        letter.innerText = "";
                    });
                    alert("Dommage tu ne le sauras pas !");
                }
            }
        } else {
            alert("Tu n'as pas compris que tu as perdu ?");
        }
    });
});
